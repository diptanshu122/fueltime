// -------- Jquery start ------- //
$(function () {
    // menu-icon - Menu toggle
    $('.menu-icon').click(function () {
        $('.header-menu').addClass('showmenu');
        setTimeout(function () {
            $('.header-menu .pin-pointer').addClass('rotateInDownRight');
        }, 400);
    });
    $('.close-main-menu').click(function () {
        $('.header-menu').removeClass('showmenu');
        setTimeout(function () {
        $('.header-menu .pin-pointer').removeClass('rotateInDownRight');
        }, 600);
    });

    // menu-list - dynemicheight
    $('.header-menu .menu-list').css('height', $('.header-menu').height() - $('.header-menu .profile-photo').height() - $('.header-menu .mention-legal').innerHeight() - 40);
    $(window).resize(function () {
        $('.header-menu .menu-list').css('height', $('.header-menu').height() - $('.header-menu .profile-photo').height() - $('.header-menu .mention-legal').innerHeight() - 40);
    });


});

// ------ country js----- //


// -------- Jquery end ------- //

